import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class HospitalService {
  private hsptl_URL:string="http://localhost:8016/hospital";
  private admin_URL: string = "http://localhost:8016/admin";
  private plasma_donor_URL: string = "http://localhost:8016/plasma-donors";
  private plasma_reciver_URL: string = "http://localhost:8016/plasma-receivers";
  private vaccination_Registration_URL: string = "http://localhost:8016/vaccinationRegistration";

  constructor(private http: HttpClient, private router: Router) { }

  getvaccinationRegistrationDetails(): Observable<any> {
    return this.http.get(this.vaccination_Registration_URL);
  }

  getPlasmaReceiverDetails(): Observable<any> {
    return this.http.get(this.plasma_reciver_URL);
  }

  displayPlasmaReceiverDetails(): Promise<any> {
    return new Promise((res, rej) => {
      this.getPlasmaReceiverDetails().subscribe(
        response => res(response),
        err => rej(err)
      )
    })
  }

  getPlasmaDonationDetails(): Observable<any> {
    return this.http.get(this.plasma_donor_URL);
  }

  displayPlasmaDonationDetail():Promise<any> {
    return new Promise((res, rej) => {
      this.getPlasmaDonationDetails().subscribe(
        response => res(response),
        err => rej(err)
      )
    })
  }

  getHsptlUserAllDetails():Observable<any>{
    return this.http.get(this.hsptl_URL);
    }

    getHsptlUserDetails(username:any):Observable<any>{
      return this.http
      .get(this.hsptl_URL + `/${username}`);
    }
    getAdminUserDetails(username:any):Observable<any>{
    return this.http.get(this.admin_URL+`/${username}`);
  }

    getAddHospitalDetails(username:any):Observable<any>{
    return this.http.post(this.hsptl_URL,username);
    }

    deleteHospitalDetails(username:any):Observable<any>{
    return this.http.delete(this.hsptl_URL+`/${username}`);
  }

  loggedIn() {
    return !!localStorage.getItem('token')
  }

  getToken() {
    return localStorage.getItem('token')
  }

  logoutUser() {
    localStorage.removeItem('token')
    this.router.navigate(['/department-login'])
  }



}



  // getHsptlUserDetails(email):Observable<any>{
  //   return this.http.get(this.hsptl_URL+`/${email}`);
  // }
  // getAdminUserDetails(email):Observable<any>{
  //   return this.http.get(this.admin_URL+`/${email}`);
  // }
  // getAddHospitalDetails(email:any):Observable<any>{
  //   return this.http.post(this.hsptl_URL,email);
  // }
